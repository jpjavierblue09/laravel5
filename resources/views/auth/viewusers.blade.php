@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Users List</div>

                <div class="card-body">
                    <table class="table table-sm-responsive table-striped table-bordered">
                        <thead>
                            <th>User</th>
                            <th>Email</th>
                            <th>Date Created</th>
                            <th>Actions</th>
                        </thead>
                        <tbody>
                             @foreach($users as $user)
                                <tr>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->created_at->diffForHumans()}}</td>
                                    <td>
                                        <a href ="/user_info/{{$user->id}}/edit" class="btn btn-primary">
                                         Edit
                                        </a>
                                        
                                        <button type="button" class="btn btn-danger"> Delete</button>
                                    </td>
                                </tr>
                            @endforeach 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
